# require.py #

[ ![Codeship Status for ont0shko/require](https://codeship.com/projects/bab4a8e0-4a76-0132-044f-0e481f3d6396/status)](https://codeship.com/projects/46340)

'require' function as is a nodejs. It can load module from dir or file.

    from require import require
    module = require('/path/to/the/module')
