# -*- coding: utf-8 -*-
# The MIT License (MIT)
#
# Copyright (c) 2013 Anton Kuzyakin <kraziant@ya.ru>
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in
# all copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE

import os, sys
import imp

def require(source):
    '''
    Функция require, позволяет загружать модули в стиле nodejs.

    module = require('/path/to/module/directory')
    '''
    path, name = os.path.split(os.path.realpath(source))
    sys.path.insert(0, path)
    path = sys.path

    fname = name.split('.')[0]

    try:
        fp, pathname, description = imp.find_module(fname, path)
    except ImportError as e:
        raise ImportError('No package named %s'%name)

    try:
        m = imp.load_module(fname, fp, pathname, description)
        return m
    finally:
        if fp:
            fp.close()
