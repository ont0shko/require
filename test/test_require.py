import os
import sys
sys.path.insert(0, os.path.split(os.path.dirname(__file__))[0])

import unittest

from require import require

class RequireTest(unittest.TestCase):
    def test_one(self):
        m = require('foo.py')
        self.assertTrue('imported' in m.foo())

if __name__ == '__main__':
    unittest.main()
